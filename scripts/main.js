'use strict';

document.addEventListener('DOMContentLoaded', function() {

    // Toggles

    var $toggler = $('.js-toggle-trigger');

    $toggler.each(function($el) {
        var _this = this;

        $(this).on('click', function(e) {
            e.stopPropagation();
            var target = $(_this).data('target');
            var $target = $('#' + target);
            $(_this).toggleClass('active');
            $target.toggleClass('active');
        });
    });

    // header menu
    var $mobileMenu = $('.header_menu');
    $mobileMenu.on('click', function(e) {
        e.preventDefault();
        $('body').toggleClass('mmenu-opened');
    });

    // close mobile menu
    $('.offcanvas_close').on('click', function() {
        $('#offcanvas').removeClass('active');
        $('body').removeClass('mmenu-opened');
        closeBottomMenu();
    });

    // mobile header bag 
    $('.js-floatbag').on('click', function(e) {
        e.stopPropagation();
        if ($(e.target).hasClass('active')) {
            $(e.target).removeClass('active');
            $('.header_cart_inner').removeClass('active');
        } else {
            $(e.target).addClass('active');
            $('.header_cart_inner').addClass('active');
            alert($('.header_cart_inner').attr('class'));
        }
    });

    $('.backToTop a').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: 0 });
    });

    // footer accordion when mobile
    if ($(window).width() < 769) {
        $('.js-footer-toggle').bind('click', footerToggle);
    }

    judgeHeader();

    $(window).on('scroll', function(e) {
        judgeHeader();
    });

    // 3 columns carousel
    $('.js-carousel').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        // centerMode: false,
        arrows: true,
        responsive: [{
            breakpoint: 2000,
            settings: {
                slidesToShow: 4,
                arrows: true
                    // centerMode: false
            }
        }, {
            breakpoint: 1009,
            settings: {
                slidesToShow: 3,
                arrows: false
                    // centerMode: false
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                centerMode: true,
                centerPadding: '17%',
                arrows: false
            }
        }]
    });

    // magnific popup
    $('.js-mfp-inline').each(function(index, ele) {
        $(ele).magnificPopup({
            type: 'inline'
        });
    });

    // close header bag
    $('body').on('click', closeFloatBag);

    function footerToggle(event) {
        event.preventDefault();
        $(this).toggleClass('active');
        $(this).next('.submenu').toggleClass('active');
    }

    function closeFloatBag(e) {
        if ($(e.target).parents('.header_cart').length <= 0) {
            if ($('#headerBag').hasClass('active')) {
                $('#headerBag').removeClass('active');
            }
        }
    }

    function judgeHeader() {
        if ($(window).scrollTop() > 118) {
            $('header').addClass('blackBg');
        } else {
            $('header').removeClass('blackBg');
        }
    }

    function closeBottomMenu() {
        $('.offcanvas_btnav .js-toggle-trigger').each(function(index, toggle) {
            var target = $(toggle).data('target');
            $(toggle).removeClass('active');
            $('#' + target).removeClass('active');
        });
    }
});

// tab
function SimpleTab(elementClass) {
    SimpleTab.TabHead = $(elementClass).find('.tab_head > a');
    SimpleTab.TabCnt = $(elementClass).find('.tab_cnt > div');
    SimpleTab.TabHead.eq(0).addClass('active');
    SimpleTab.TabCnt.eq(0).addClass('active');
    SimpleTab.TabHead.on('click', function(e) {
        e.preventDefault();
        var toActiveIndex = $(e.target).index();
        SimpleTab.TabHead.removeClass('active');
        $(e.target).addClass('active');
        SimpleTab.TabCnt.removeClass('active');
        SimpleTab.TabCnt.eq(toActiveIndex).addClass('active');
    });
}

$(function() {
    //footer選單
    $(".footer_menu").click(function() {
        if ($(this).find(".footer_submenu").hasClass("show")) {
            $(this).find(".footer_submenu").removeClass("show");
        } else {
            if ($(this).hasClass("toStaticMenu")) {
                $(".footer_menu .footer_submenu").removeClass("show"); //關閉footer選單
                $(".wrap").toggleClass("openStaticMenu"); //開關footer-static選單
            } else {
                $(".footer_menu .footer_submenu").removeClass("show"); //關閉footer選單
                $(".wrap").removeClass("openStaticMenu"); //關閉footer-static選單
                $(".staticMenu li").removeClass("show");
                $(this).find(".footer_submenu").addClass("show");
            }
        }
    });

    //staticMenu-子選單 & member-m 下拉選單 & static-m 下拉選單
    $(".js-menu").click(function() {
        if ($(this).parent().hasClass("show")) {
            $(this).parent().removeClass("show");
        } else {
            $(".staticMenu li").removeClass("show");
            $(this).parent().addClass("show");
        }
    });
})